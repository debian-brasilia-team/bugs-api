#!/usr/bin/env python3

import uvicorn
from os import getenv

PORT = int(getenv('PORT', 8080))
RELOAD = getenv('RELOAD', False)
LOG_LEVEL = getenv('LOG_LEVEL', 'INFO').upper()


if __name__ == '__main__':
    log_config = uvicorn.config.LOGGING_CONFIG
    log_config["formatters"]["access"]["fmt"] = "%(asctime)s %(levelname)s [%(name)s] [%(filename)s:%(lineno)d] - %(message)s"
    uvicorn.run(
        'src:app',
        host='0.0.0.0',
        port=PORT,
        reload=RELOAD,
        log_config=log_config
    )
