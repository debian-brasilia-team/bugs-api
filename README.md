# bugs-api

A lightweight API to **query** and **parse** the [Debian bug tracking system (BTS)](bugs.debian.org).

## Dependencies

- Podman
- Python >= 3
- Make

```
apt install python3-venv python3-pip python3-dev podman
```

## Development

| Name   | Command       | Description                                                       |
|--------|---------------|-------------------------------------------------------------------|
| Build  | `make build`  | Build the Dockerfile into a Docker image.                         |
| Run    | `make run`    | Execute the entrypoint `main.py` and start the uvicorn webserver. |
| Lint   | `make lint`   | Check lint flake8 lint policies defined at `.flake8`.             |
| Push   | `make push`   | Push Image to the docker registry.
| Shell  | `make shell`  | Init a container but with shell attached for debug.               |
| Freeze | `make freeze` | Lock pip dependencies to `requirements.txt` file.                 |
| Clean  | `make clean`  | Clean cached folders and files.                                   |


## Deploy

```bash
cat > production.env << EOF
RELOAD=False
PORT=8080
LOG_LEVEL=INFO

EOF
```

```bash
podman login registry.salsa.debian.org

podman run -d \
    --name bugs-api \
    --restart unless-stopped \
    --network host \
    --env-file ./production.env \
    registry.salsa.debian.org/debian-brasilia-team/bugs-api:latest
```


#### Setup and Run manually

```bash
# Create environment
python3 -m venv venv

# To activate the environment
source venv/bin/activate

python3 -m pip install -r requirements.txt

python3 main.py

# When you finish you can exit typing
deactivate

# Lock pip dependencies to requirements.txt file
python3 -m pip freeze > requirements.txt
```

## License

This API is licensed under the Apache 2 License. For more details, please see [LICENSE](https://salsa.debian.org/debian-brasilia-team/bugs-api/blob/main/LICENSE).
