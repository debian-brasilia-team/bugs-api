import base64
import email
from src.mbox_reader import MboxReader


def unpack_attachment(message):
    if message.get_content_disposition() != 'attachment':
        return None

    attachment = {
        'Filename': message.get_filename(),
        'Payload': message.get_payload()
    }

    for key in message.keys():
        attachment[key] = message.get(key)

    return attachment


def enpack_headers(content):
    headers = {}

    # Unpack Headers
    for key in content.keys():
        headers[key] = content.get(key)

    return headers


def mbox_to_api(handle, encoding: str = 'text'):
    items = []

    with MboxReader(handle) as mbox:
        for content in mbox:
            messages = content.get_payload()

            # Normalise in mail message objects
            if type(content.get_payload()) == str:
                email_obj = email.message_from_string(content.get_payload())
                messages = [email_obj]

            attachments = []
            payloads = []

            for message in messages:
                payload = {}

                for key in message.keys():
                    payload[key] = message.get(key)

                if message.get_content_disposition() != 'attachment':
                    temp_message = message.get_payload()

                    if encoding == 'text':
                        payload['Message'] = temp_message
                    elif encoding == 'base64':
                        payload_string_bytes = temp_message.encode("ascii")
                        base64_bytes = base64.b64encode(payload_string_bytes)
                        base64_string = base64_bytes.decode("ascii")
                        payload['Message'] = base64_string
                    else:
                        raise Exception('Encoding not implemented')

                    payloads.append(payload)

                attachment = unpack_attachment(message)
                if attachment:
                    attachments.append(unpack_attachment(message))

            message = {
                'Headers': enpack_headers(content)
            }

            message['Payload'] = payloads
            message['Attachments'] = attachments

            items.append(message)

    return items
