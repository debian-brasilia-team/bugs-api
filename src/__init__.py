from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from prometheus_fastapi_instrumentator import Instrumentator
from fastapi.responses import RedirectResponse
from src.api.v1 import api_router
from os import getenv

LOG_LEVEL = getenv('LOG_LEVEL', 'INFO').upper()
VERSION = getenv('VERSION')
DEBUG = True if LOG_LEVEL == "DEBUG" else False


description = """
A lightweight API to **query** and **parse** the [Debian bug tracking system (BTS)](bugs.debian.org). 🚀
"""

application = FastAPI(
    title='Bugs Debian API',
    description=description,
    debug=DEBUG,
    version=VERSION,
    contact={
        'name': 'Debian Brasilia Team',
        'url': 'https://brasilia.debian.net',
        'email': 'debianbsb@gmail.com',
    },
    license_info={
        'name': 'Apache 2.0',
        'url': 'https://www.apache.org/licenses/LICENSE-2.0.html',
    },
)


def configure_routes():
    application.include_router(api_router)


def configure_instrumentation():
    Instrumentator(
        should_group_status_codes=False,
        should_ignore_untemplated=True,
        should_respect_env_var=False,
        should_instrument_requests_inprogress=True,
        excluded_handlers=['/metrics', '/docs'],
        env_var_name='ENABLE_METRICS',
        inprogress_name='inprogress',
        inprogress_labels=True,
    ).instrument(application).expose(application, include_in_schema=False)


def configure_cors():
    origins = [
        'http://localhost:8000',
        'https://bugs.debianbsb.org'
    ]

    application.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['GET'],
        allow_headers=['*']
    )


def get_app() -> FastAPI:
    configure_routes()
    configure_instrumentation()
    configure_cors()

    return application


app = get_app()


@app.get('/', include_in_schema=False)
async def redirect_docs():
    return RedirectResponse(url='/docs')
