from fastapi import APIRouter
from .endpoints import router


api_router = APIRouter(prefix="/api/v1")

api_router.include_router(router.router, tags=["bug"])
