def validate_params(params):
    if params.page < 1:
        params.page = 1

    if params.size < 1:
        params.size = 1

    if params.size > 100:
        params.size = 100

    return params
