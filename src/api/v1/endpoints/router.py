import io
import math
import requests

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse

from .document import doc_200, doc_404
from .query import Params
from .helpers import validate_params
from src.helpers import mbox_to_api


router = APIRouter(prefix="/bugs")


@router.get('/{bug_id}', name='Get bug using id',  responses={
    200: doc_200,
    404: doc_404
})
async def bug(bug_id: str, params: Params = Depends()):
    params = validate_params(params)

    try:
        response = requests.get(
            f'https://bugs.debian.org/cgi-bin/bugreport.cgi?bug={bug_id};mbox=yes'
        )
        response.raise_for_status()
    except requests.exceptions.HTTPError as error:
        print(f"HTTP Error: {error}")
        if response.status_code == 404:
            return JSONResponse(status_code=404, content={"message": "Bug not found"})

        raise JSONResponse(status_code=response.status_code, detail=str(error))
    except requests.exceptions.RequestException as error:
        print(f"Request Exception: {error}")
    except Exception as error:
        print(f"An error occurred: {error}")

    io_file = io.BytesIO(response.content)
    items = mbox_to_api(io_file, params.encoding)

    total = len(items)
    pages = 1

    if total > params.size:
        pages = math.ceil(total / params.size)

        start_slice = ((params.page - 1) * params.size) - 1
        end_slice = start_slice + params.size

        if end_slice > total:
            end_slice = total - 1
        items = items[start_slice:end_slice]

    response = {
        'items': items,
        'total': total,
        'page': params.page,
        # TODO: Don't know if I return the size of items of the size requested in the query
        'size': len(items),
        'pages': pages
    }

    return response
