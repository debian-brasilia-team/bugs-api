from .models import Message


doc_200 = {
    "description": "Item requested by ID",
    "content": {
        "application/json": {
            "example": {
                "items": [
                    {
                        "Headers": {
                            "Received": "(at submit) by bugs.debian.org; 00 Jan 0000 00:00:00 +0000",
                            "X-Spam-Checker-Version": "SpamAssassin 3.4.6-bugs.debian.org_2005_01_02\t(2021-04-09) on buxtehude.debian.org",
                            "X-Spam-Level": "",
                            "X-Spam-Status": "No, score=-11.1 required=4.0 tests=BAYES_00,FOURLA,HAS_PACKAGE,\tRDNS_NONE,SPF_NONE,T_SCC_BODY_TEXT_LINE,XMAILER_REPORTBUG\tautolearn=ham autolearn_force=no\tversion=3.4.6-bugs.debian.org_2005_01_02",
                            "X-Spam-Bayes": "score:0.0000 Tokens: new, 28; hammy, 150; neutral, 91; spammy,\t0. spammytokens: hammytokens:0.000-+--python3, 0.000-+--H*M:reportbug,\t0.000-+--H*MI:reportbug, 0.000-+--cutf8, 0.000-+--CUTF8",
                            "Return-path": "",
                            "Content-Type": "text/plain; charset=\"us-ascii\"",
                            "MIME-Version": "1.0",
                            "Content-Transfer-Encoding": "7bit",
                            "From": "",
                            "To": "Debian Bug Tracking System <submit@bugs.debian.org>",
                            "Subject": "",
                            "Message-ID": "",
                            "X-Mailer": "reportbug 12.0.0",
                            "Date": "Sun, 00 Jan 0000 00:00:00 +0000",
                            "Delivered-To": "submit@bugs.debian.org"
                        },
                        "Payload": [
                            {
                                "Package": "package",
                                "Version": "0.0.0-0",
                                "Severity": "serious",
                                "Message": ""
                            }
                        ],
                        "Attachments": []
                    },
                ]
            }
        }
    }
}

doc_404 = {
    "model": Message,
    "description": "The bug was not found using the id.",
    "content": {
        "application/json": {"example": {"message": "Bug not found"}}
    }
}
