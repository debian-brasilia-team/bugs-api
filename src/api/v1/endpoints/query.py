from fastapi import Query


class Params:
    def __init__(
        self,
        encoding: str = Query(default='text', description="Encoding for the email payload messages. It can be 'text' (default) or 'base64'."),
        page: int = Query(default=1, description="Number of the page to start the request."),
        size: int = Query(default=20, description="Size of items on each page. Min: 1 - Max: 20"),
    ):
        self.encoding = encoding
        self.page = page
        self.size = size
