FROM python:3.11-alpine3.16

WORKDIR /app

COPY ./requirements.txt requirements.txt

RUN pip install --no-cache-dir --upgrade pip -r requirements.txt

COPY ./ /app/

EXPOSE 8080

CMD [ "python", "main.py" ]
